<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include "header.php"; ?>
	</head>
	<body class="no-skin">
		<?php include "header-top.php"; ?>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<?php include "sidebar.php"; ?>
			<?php 
				if($_GET['var'] == 'dashboard'){
					include "content/dashboard.php";
				}else if($_GET['var'] == 'input-news'){
					include "content/i-news.php";
				}else if($_GET['var'] == 'profile'){
					include "content/profile.php";
				}
			?>
			<?php include "footer.php"; ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div>
		<?php include "footer-js.php"; ?>
	</body>
</html>
